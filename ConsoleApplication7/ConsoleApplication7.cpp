﻿// ConsoleApplication7.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#ifndef STACK

#include <cassert>
#include <iostream>

#include <iomanip>

template <typename T>
class Stack {
private:
  T* stackPtr;
  const int size;
  int top;
public:
  Stack(int = 10);
  ~Stack();

  inline void push(const T&);
  inline void pop();
  inline void printStack();
};

template <typename T>
Stack<T>::Stack(int maxSize) : size(maxSize) {
  stackPtr = new T[size]; 
  top = 0;
}

template <typename T>
inline void Stack<T>::push(const T& value) {
  assert(top < size); 
  stackPtr[top++] = value;
}

template <typename T>
inline void Stack<T>::pop() {
  assert(top > 0); 
  stackPtr[--top];
}

template <typename T>
inline void Stack<T>::printStack() {
  for (int ix = top - 1; ix >= 0; ix--)
    std::cout << stackPtr[ix] << std::endl;
}


template <typename T>
Stack<T>::~Stack()
{
  delete[] stackPtr;
}



int main()
{
    // std::cout << "Hello World!\n";
  Stack<char> stackSymbol(5);
  int ct = 0;
  char ch;

  while (ct++ < 5)
  {
    std::cin >> ch;
    stackSymbol.push(ch);
  }

  std::cout << std::endl;

  stackSymbol.printStack(); 

  std::cout << std::endl;

  stackSymbol.pop();

  stackSymbol.printStack();

  return 0;

}
#endif


// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
